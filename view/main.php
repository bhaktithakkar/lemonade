<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$json =  file_get_contents("../list.json");
$data = json_decode($json)->cats;
function sortById($a,$b){
    return $a->id - $b->id;
}
uasort($data, 'sortById');
$i=1;
?>
    <div class="container-fluid bg-3 text-center">    
        <h3>Featured</h3><br>
        <?php foreach ($data as $key => $value   ){
            if($i % 2 != 0 ){
        ?>        
        <div class="row">
            <div class="col-sm-6">
                <img src="<?php echo $value->image;?>" class="img-responsive" style="width:100%" alt="Image">
                <p id="header"><?php echo $value->heading;?></p>
                <p id="content"><?php echo $value->content;?></p>
            </div> 
        <?php  }
            else {?>
                <div class="col-sm-6">
                    <img src="<?php echo $value->image;?>" class="img-responsive" style="width:100%" alt="Image">
                    <p id="header"><?php echo $value->heading;?></p>
                    <p id="content"><?php echo $value->content;?></p>
                </div> 
        </div><br><br>
      <?php  }
      
        $i++; }?>
    </div>
        